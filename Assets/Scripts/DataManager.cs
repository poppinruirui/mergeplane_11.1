﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {

    public static DataManager s_Instance = null;


    public static string url = "http://39.108.53.61:5566/data/config1/";


    // 交通工具
    Dictionary<string, int> m_dicCoinGainPerRound = new Dictionary<string, int>(); // 每走完一圈获得金币
    Dictionary<string, int> m_dicCoinVehiclePrice = new Dictionary<string, int>(); // 交通工具的售价
    float[] m_aryRoundTimeByLevel = new float[40]; // 交通工具绕一圈的时间


    Dictionary<string, int> m_dicCoinCostToPrestige = new Dictionary<string, int>(); // 每个赛道的重生价格

    Dictionary<string, sAutomobileConfig> m_dicAutomobileConfig = new Dictionary<string, sAutomobileConfig>();
    Dictionary<int, sPlanetConfig> m_dicPlanetConfig = new Dictionary<int, sPlanetConfig>();
    Dictionary<string, sTrackConfig> m_dicTrackConfig = new Dictionary<string, sTrackConfig>();

    Dictionary<string, string> m_dicTrackAndLevel2ResId = new Dictionary<string, string>();

    public enum eMoneyType
    {
        legal_currency, // 法币
        diamond,        // 钻石
        planet_0_coin,  // 青铜星金币
        planet_1_coin,  // 白银星金币
        planet_2_coin,  // 黄金星金币
    }; 

    // 载具配置
    public struct sAutomobileConfig
    {
        public int nId;
        public int nLevel; // 本交通工具的等级
        public int nResId;
        public string szName;
        public eMoneyType eCoinType; // 召唤价格（起始价格） - 金币类型 0 - 青铜星 1 - 白银星  2 - 黄金星
        public double nStartPrice_CoinValue; // 召唤价格（起始价格） - 金币
        public float fPriceRaisePercentPerBuy; // 每次购买之后价格上涨百分比
        public int nPriceDiamond;  // 购买价格 - 钻石(购买之后价格不增加)
        public int nCanUnlockLevel; // 可解锁等级
        public string szSuitableTrackId; // 可适配的赛道Id
        public double nBaseGain;  // 基础收益：跑完一圈赚多少金币
        public float fBaseSpeed; // 跑完一圈需要多少秒


    }; // end sAutomobileConfig
    sAutomobileConfig tempAutomobileConfig;

    // 星球配置
    public struct sPlanetConfig
    {
        public string szName;    // 名称 
        public int nId;          // 唯一Id
        public double nUnlockPrice; // 解锁价格
        public eMoneyType eUnlockMoneyType; // 解锁所需的货币类型
    };
    sPlanetConfig tempPlanetConfig;


    // 赛道配置
    public struct sTrackConfig
    {
        public int nId;       // 赛道的Id
        public string szName; // 赛道的名字
        public int nPlanetId; // 所属星球Id
        public double nUnlockPrice;// 解锁本赛道所需花费的金币数量
        public float fEarning; // 基础收益倍数
        public double nBuyAdminBasePrice; // 招募主管的基础价格（即起始价格)

    };
    sTrackConfig tempTrackConfig;

    private void Awake()
    {
        s_Instance = this;

    }

    // Use this for initialization
    void Start () {
        Init();


      
      
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int PlanetId2CoinId( int nPlanetId  )
    {
        int nCoinId = (int)eMoneyType.planet_0_coin;
        switch (nPlanetId)
        {
            case 0:
                {
                    nCoinId = (int)eMoneyType.planet_0_coin;
                }
                break;
            case 1:
                {
                    nCoinId = (int)eMoneyType.planet_1_coin;
                }
                break;
            case 2:
                {
                    nCoinId = (int)eMoneyType.planet_2_coin;
                }
                break;
        } // swith

        return nCoinId;
    }

    IEnumerator LoadConfig_Planet(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            sPlanetConfig config = new sPlanetConfig();

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            config.nId = nId;
            config.szName = aryParams[1];
            config.nUnlockPrice = double.Parse(aryParams[2]);

            m_dicPlanetConfig[config.nId] = config;
        }

    } // end LoadConfig_Planet


    IEnumerator LoadConfig_Track(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载
        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            sTrackConfig config = new sTrackConfig();

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            config.nId = nId;
            int nColIndex = 1;
            config.szName = aryParams[nColIndex++];
            config.nPlanetId = int.Parse(aryParams[nColIndex++]);
            config.nUnlockPrice = double.Parse(aryParams[nColIndex++]);
            config.fEarning = float.Parse(aryParams[nColIndex++]);
            config.nBuyAdminBasePrice = double.Parse(aryParams[nColIndex++]);

            string szKey = config.nPlanetId + "_" + config.nId;
            m_dicTrackConfig[szKey] = config;
        } // end for i


        MapManager.s_Instance.NewZengShouCounter(0, 0); // poppin to do 写在这里很不规范，稍后修改一下
    } // end LoadConfig_Track

    IEnumerator LoadConfig_Automobile(string szFileName)
    {         WWW www = new WWW(szFileName);         yield return www; // 等待下载
        string[] aryLines =  www.text.Split( '\n' );
        for (int i = 1; i < aryLines.Length; i++ )
        {
            string[] aryParams = aryLines[i].Split( ',' );
            sAutomobileConfig config = new sAutomobileConfig();

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            config.nId = nId;
            config.szName = aryParams[1];

            string szResId = aryParams[2];
          //  config.nResId = int.Parse(aryParams[2]);
            config.nStartPrice_CoinValue = 0;
            if ( !double.TryParse(aryParams[3], out config.nStartPrice_CoinValue ) )
            {
                config.nStartPrice_CoinValue = 0;
            }
            config.fPriceRaisePercentPerBuy = float.Parse(aryParams[4]);
           // config.nStartPrice_CoinType = int.Parse(aryParams[5]);

           

            config.nPriceDiamond = int.Parse(aryParams[5]);
            config.nLevel = int.Parse(aryParams[6]);
            config.nCanUnlockLevel = int.Parse(aryParams[7]);
            config.szSuitableTrackId = aryParams[8];

            //  购买交通工具所需的金币类型，不用配置，而是根据该交通工具适用的星球来决定的
            string[] aryTemp = config.szSuitableTrackId.Split( '_' );
            int nPlanetId = int.Parse(aryTemp[0]);
            switch(nPlanetId)
            {
                case 0:
                    {
                        config.eCoinType = eMoneyType.planet_0_coin;
                    }
                    break;
                case 1:
                    {
                        config.eCoinType = eMoneyType.planet_1_coin;
                    }
                    break;
                case 2:
                    {
                        config.eCoinType = eMoneyType.planet_2_coin;
                    }
                    break;
            }// end switch


            config.nBaseGain = 0;//
            if ( !double.TryParse(aryParams[9], out config.nBaseGain))
            {
                config.nBaseGain = 0;
            }
            config.fBaseSpeed = float.Parse(aryParams[10]);

            string szResIdKey = config.szSuitableTrackId + "_" + config.nLevel;
            m_dicTrackAndLevel2ResId[szResIdKey] = szResId;

            string szKey = config.szSuitableTrackId + "_" + config.nLevel;
            m_dicAutomobileConfig[/*config.nId*/szKey] = config;
        }


    }

    public Dictionary<string, sAutomobileConfig> GetAutomobileConfig()
    {
        return m_dicAutomobileConfig;
    }


        public void Init()
    {
        string szConfigFileName_Automobile = url + "automobile.csv";
        string szConfigFileName_Planet = url + "planet.csv";
        string szConfigFileName_Track = url + "track.csv";
        StartCoroutine( LoadConfig_Automobile (szConfigFileName_Automobile));
        StartCoroutine(LoadConfig_Planet(szConfigFileName_Planet));
        StartCoroutine(LoadConfig_Track(szConfigFileName_Track));


        // 交通工具每走一圈所获得的金币
        for (int i = 0; i < 3; i++ ) // planet
        {
            for (int j = 0; j < 5; j++ ) // district
            {
                for (int k = 1; k <= 40;  k++ ) // level
                {
                    int nValue = k  * ( j + 1 ) * ( i + 1 );
                    nValue *= 100;
                    string szKey = i + "_" + j + "_" + k;
                    m_dicCoinGainPerRound[szKey] = nValue;
                } // end for k
            } // end for j
        } // end for i

        // 交通工具的售价
        for (int i = 0; i < 3; i++) // planet
        {
            for (int j = 0; j < 5; j++) // district
            {
                for (int k = 1; k <= 40; k++) // level
                {
                    int nValue = k * (j + 1) * (i + 1) * 100;
                    string szKey = i + "_" + j + "_" + k;
                    m_dicCoinVehiclePrice[szKey] = nValue;
                } // end for k
            } // end for j
        } // end for i



        // 每一个赛道重生所需要的金币
        for (int i = 0; i < 3; i++) // planet
        {
            for (int j = 0; j < 5; j++) // district
            {
                    int nValue = 10000 * (j + 1) * (i + 1);
                    string szKey = i + "_" + j;
                    m_dicCoinCostToPrestige[szKey] = nValue;
            } // end for j
        } // end for i

        for (int i = 0; i < 40; i++)
        {
            m_aryRoundTimeByLevel[i] = 6f - 0.2f * i;
        }
        // poppin to discuss
        // 随着重生次数增加，同一个赛道的重生花费应该不同吧？？

        // poppin to discuss 
        // 不同的星球和赛道，重生之后的收益数值是否应该不同


    }

    // right here
    // 获取：交通工具每走一圈所获得的金币
    public double GetCoinGainperRound( int nPlanetId, int nDistrictId, int nLevel )
    {

        /*
        string szKey = nPlanetId + "_" + nDistrictId + "_" + nLevel;
     
        if ( !m_dicCoinGainPerRound.TryGetValue( szKey, out nValue ) )
        {
            nValue = 0;
        }
        */
        double nAutomobileEarning = DataManager.s_Instance.GetAutomobileConfig(nPlanetId, nDistrictId, nLevel).nBaseGain;
        float fTrackRaise = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nDistrictId).fEarning;
        nAutomobileEarning =  nAutomobileEarning * fTrackRaise ;

        return nAutomobileEarning;
    }

    // 获取：交通工具的售价
    public int GetVehiclePrice(int nPlanetId, int nDistrictId, int nLevel)
    {
        string szKey = nPlanetId + "_" + nDistrictId + "_" + nLevel;
        int nValue = 0;
        if (!m_dicCoinVehiclePrice.TryGetValue(szKey, out nValue))
        {
            nValue = 0;
        }

        // 根据购买次数，价格会增加
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        District district = planet.GetDistrictById(nDistrictId);
        int nBuyTimes = district.GetVehicleBuyTimes(nLevel);

        // 公式是瞎鸡巴乱填的，后期数值策划来弄 
        if (nBuyTimes > 0)
        {
            nValue *= ( nBuyTimes + 1 );
        }


        return nValue;
    }

    public float GetVehicleRunTimePerRound( int nLevel )
    {
        int nIndex = nLevel;
        return m_aryRoundTimeByLevel[nIndex];
    }

    public int GetPrestigaeCoinCost(int nPlanetId, int nDistrictId )
    {
        string szKey = nPlanetId + "_" + nDistrictId;
        int nValue = 0;

        if (!m_dicCoinCostToPrestige.TryGetValue(szKey, out nValue))
        {
            nValue = 0;
        }
        return nValue;
    }
   
    public int GetPrestigeGain(int nPlanetId, int nDistrictId, int nCurPrestigeTimes)
    {
        if (nCurPrestigeTimes == 0)
        {
            return 1;
        }
        return nCurPrestigeTimes * 2;
    }

    public double GetPlanetUnlockCostById( int nPlanetId )
    {
        if ( !m_dicPlanetConfig.TryGetValue( nPlanetId, out tempPlanetConfig) )
        {
            Debug.LogError(" !m_dicPlanetConfig.TryGetValue( nPlanetId, out nCost )");
        }

        return tempPlanetConfig.nUnlockPrice;
    }

    // 根据星球的Id号获取该星球的配置数值
    public sPlanetConfig GetPlanetConfigById( int nPlanetId )
    {
        if (!m_dicPlanetConfig.TryGetValue(nPlanetId, out tempPlanetConfig))
        {
            Debug.LogError(" !m_dicPlanetConfig.TryGetValue( nPlanetId, out nCost )");
        }

        return tempPlanetConfig;
    }

    // 根据赛道的Id号获取该赛道的配置数值
    public sTrackConfig GetTrackConfigById(int nPlanetId, int nTrackId )
    {
        string szKey = nPlanetId + "_" + nTrackId;
        if (!m_dicTrackConfig.TryGetValue(szKey, out tempTrackConfig))
        {
           // Debug.LogError(" !m_dicPlanetConfig.TryGetValue( nPlanetId, out nCost )");
        }

        return tempTrackConfig;
    }

    // 根据星球Id、赛道Id、载具等级，获取载具配置
    public sAutomobileConfig GetAutomobileConfig( int nPlanetId, int nTrackId, int nAutoLevel )
    {
        string szKey = nPlanetId + "_" + nTrackId + "_" + nAutoLevel;
        if ( !m_dicAutomobileConfig.TryGetValue(szKey, out tempAutomobileConfig) )
        {
            Debug.LogError("GetAutomobileConfig");
        }

        return tempAutomobileConfig;
    }

} // end class
