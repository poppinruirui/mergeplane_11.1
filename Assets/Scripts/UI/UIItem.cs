﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItem : MonoBehaviour {

    public bool m_bInBag = false;


    public GameObject _panelCounting;

    public Text _txtName;
    public Text _txtDesc;
    public Text _txtValue;

    public Text _txtPrice_Coin;
    public Text _txtPrice_Diamond;

    /// <summary>
    /// price and buy
    /// </summary>
    public Text _txtPrice1;
    public Text _txtPrice2;

    public Image _imgMoneyIcon1;
    public Image _imgMoneyIcon2;

    public GameObject _goPrice1;
    public GameObject _goPrice2;

    // end price and buy

    public Text _txtNum;
    public Text _txtLeftTime;
   
    public Button _btnUse;

    public ShoppinMall.eItemType m_eItemType;
    public ShoppinMall.ePriceType m_ePriceType;
    public ShoppinMall.ePriceSubType m_ePriceSubType;


    public int m_nItemId = 0;
    public double m_nPrice_Coin = 0;
    public float m_fCoinPriceRiseAfterBuy = 0f;
    public double m_nPrice_Diamond = 0;
    public int m_nValue0 = 0;
    public int m_nDuration = 0;

    public float m_fStartTime = 0;

    public ShoppinMall.sShoppingMallItemConfig m_Config; // 商城表里的配置(shoppingmall.csv)
    public ItemSystem.sItemConfig m_BagItemConfig;       // 道具表里的配置(item.csv)

    int m_nNum = 0;

    int[] m_aryIntParams = new int[8];
    float[] m_aryFloatParams = new float[8];
    int m_nLeftTime = 0;

    // Use this for initialization
    void Start () {

        /*
        _txtPrice_Coin.text = m_nPrice_Coin.ToString();
        _txtPrice_Diamond.text = m_nPrice_Diamond.ToString();

        if ( m_eItemType == ShoppinMall.eItemType.diamond )
        {
            _txtPrice_Coin.text = "¥" + _txtPrice_Coin.text;
        }
        */
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetConfig(ShoppinMall.sShoppingMallItemConfig config)
    {
        m_Config = config;
        m_BagItemConfig = ItemSystem.s_Instance.GetItemConfigById(config.nId);

        _txtName.text = m_BagItemConfig.szName;
    }

   


    // 常规的价格设置流程
    public void SetPrice( ShoppinMall.sShoppingMallItemConfig config )
    {
        if ( config.nPriceType2 == -1 ) // 没有第二类价格
        {
            _goPrice2.SetActive( false );
        }
        else
        {
            _goPrice2.SetActive(true);
        }

   
        /// price1
        ShoppinMall.ePriceType eType1 = (ShoppinMall.ePriceType)config.nPriceType1;
        switch (eType1)
        {
            case ShoppinMall.ePriceType.legal_tender: // 现实世界的法币
                {
                    _imgMoneyIcon1.sprite = ResourceManager.s_Instance.GetCoinSpriteByType(eType1);
                }
                break;
            case ShoppinMall.ePriceType.green_cash: // 钻石/绿票/超级现金
                {
                    _imgMoneyIcon1.sprite = ResourceManager.s_Instance.GetCoinSpriteByType(eType1);
                }
                break;
            case ShoppinMall.ePriceType.coin: // 金币/游戏币
                {
                    _imgMoneyIcon1.sprite = ResourceManager.s_Instance.GetCoinSpriteByType(eType1, config.nPriceSubType1);
                }
                break;
        } // end switch

        _txtPrice1.text = config.nPrice1.ToString("f0");

        /// end price1

        /// price 2
        ShoppinMall.ePriceType eType2 = (ShoppinMall.ePriceType)config.nPriceType2;
        switch (eType2)
        {
            case ShoppinMall.ePriceType.legal_tender: // 现实世界的法币
                {
                    _imgMoneyIcon2.sprite = ResourceManager.s_Instance.GetCoinSpriteByType(eType2);
                }
                break;
            case ShoppinMall.ePriceType.green_cash: // 钻石/绿票/超级现金
                {
                    _imgMoneyIcon2.sprite = ResourceManager.s_Instance.GetCoinSpriteByType(eType2);
                }
                break;
            case ShoppinMall.ePriceType.coin: // 金币/游戏币
                {
                    _imgMoneyIcon2.sprite = ResourceManager.s_Instance.GetCoinSpriteByType(eType2, config.nPriceSubType2);
                }
                break;
        } // end switch

        _txtPrice2.text = config.nPrice2.ToString("f0");

        // end price2

        if ( eType1 == ShoppinMall.ePriceType.coin )
        {
            m_nPrice_Coin = config.nPrice1;
        }
        else if (eType2 == ShoppinMall.ePriceType.coin)
        {
            m_nPrice_Coin = config.nPrice2;
        }
        m_fCoinPriceRiseAfterBuy = config.fPriceRiseEachBuy;
        int nBuyTime = ShoppinMall.s_Instance.GetItemBuyTime( config.nId );
        if (m_fCoinPriceRiseAfterBuy > 0)
        {
            for (int i = 0; i < nBuyTime; i++)
            {
                m_nPrice_Coin *= (1 + m_fCoinPriceRiseAfterBuy);
            }
        }
       
        if (eType1 == ShoppinMall.ePriceType.coin)
        {
            _txtPrice1.text = CyberTreeMath.GetFormatMoney(m_nPrice_Coin) ;//m_nPrice_Coin.ToString("f0");
        }
        else if (eType2 == ShoppinMall.ePriceType.coin)
        {
            _txtPrice2.text = CyberTreeMath.GetFormatMoney(m_nPrice_Coin);//m_nPrice_Coin.ToString("f0");
        }

    }

    public void OnClickButton_DoSomething()
    {
        if ( m_bInBag )
        {
            OnClickButton_Use();
        }
        else
        {
            OnClickButton_Buy();
        }
    }

    public void OnClickButton_Buy_UseCoin()
    {
        // DoBuy(ShoppinMall.ePriceType.coin);
        DoBuy_ByMoneyType(1);
    }

    public void OnClickButton_Buy_UseDiamond()
    {

        // DoBuy(ShoppinMall.ePriceType.green_cash);
        DoBuy_ByMoneyType(2);  
    }

    ShoppinMall.ePriceType m_eToBuy_MoneyType = ShoppinMall.ePriceType.coin;
    int m_nToBuy_SubMoneyType = 0;
    double m_nToBuy_Price = 0;
    int m_nToBuy_Num = 0;
    ShoppinMall.sShoppingMallItemConfig m_ToBuyConfig;
    public void DoBuy_ByMoneyType( int nTypeNo )
    {
        if ( nTypeNo == 1 )
        {
            m_eToBuy_MoneyType = (ShoppinMall.ePriceType)m_Config.nPriceType1;
            m_nToBuy_SubMoneyType = m_Config.nPriceSubType1;
            m_nToBuy_Price = m_Config.nPrice1;
            m_nToBuy_Num = m_Config.nNum1;
        }
        else if (nTypeNo == 2)
        {
            m_eToBuy_MoneyType = (ShoppinMall.ePriceType)m_Config.nPriceType2;
            m_nToBuy_SubMoneyType = m_Config.nPriceSubType2;
            m_nToBuy_Price = m_Config.nPrice2;
            m_nToBuy_Num = m_Config.nNum2;
        }
        m_ToBuyConfig = m_Config;

        if (m_eToBuy_MoneyType == ShoppinMall.ePriceType.legal_tender)
        {
            UIMsgBox.s_Instance.ShowMsg("法币购买接口尚未开通");
            return;
        }

        // 用金币购买的，直接执行购买操作；用钻石购买的，需要弹一个确认框
        if (m_eToBuy_MoneyType == ShoppinMall.ePriceType.coin) // 金币购买
        {
            // 判断相应的金币够不够，够就直接买了；不够就给出系统提示 
            double nCurCoinNum = AccountSystem.s_Instance.GetCoin(m_nToBuy_SubMoneyType);
            if (nCurCoinNum < m_nPrice_Coin)
            {
                UIMsgBox.s_Instance.ShowMsg( "金币不足!" );
                return;
            }

            // 扣金币, 购买成功,弹出提示框”要不要立即使用“
            nCurCoinNum -= m_nPrice_Coin;

            AccountSystem.s_Instance.SetCoin(m_nToBuy_SubMoneyType, nCurCoinNum);

            UIMsgBox.s_Instance.ShowMsg( "购买成功！" );

            ShoppinMall.s_Instance.BuySucceed( this, m_nToBuy_Num);

            // 如果配置了“每次购买之后金币价格上涨”，则上涨。(目前只有“天赋点”这一类商品会走这个流程)
            if ( m_ToBuyConfig.fPriceRiseEachBuy > 0 )
            {
                int nBuyTimes = ShoppinMall.s_Instance.GetItemBuyTime(m_ToBuyConfig.nId);
                nBuyTimes++;
                ShoppinMall.s_Instance.SetItemBuyTime(m_ToBuyConfig.nId, nBuyTimes);
                SetPrice(m_ToBuyConfig);
            }
 




        }
        else if (m_eToBuy_MoneyType == ShoppinMall.ePriceType.green_cash) // 钻石购买 
        {
            ShoppinMall.s_Instance.ShowConfirmBuyPanel( this, m_nToBuy_Price, m_nToBuy_Num, m_Config, m_BagItemConfig);
        }
        
    }

   
    public void SetPriceVisible( bool bVisible )
    {
        _goPrice1.SetActive(bVisible);
        _goPrice2.SetActive(bVisible);
    }

    public void SetLeftTimeVisible(bool bVisible)
    {
        _txtLeftTime.gameObject.SetActive(bVisible);
    }

    public void SetNumVisible(bool bVisible)
    {
        _txtNum.gameObject.SetActive(bVisible);
    }

    public void DoBuy(ShoppinMall.ePriceType ePriceType )
    {
        if (m_eItemType == ShoppinMall.eItemType.diamond)
        {
            UIMsgBox.s_Instance.ShowMsg("此功能暂未开放");
            return;
        }

        double nCurHave = 0;
        double nPrice = 0;
        string szMoneyType = "";
        switch (ePriceType)
        {
            case ShoppinMall.ePriceType.green_cash:
                {
                    nCurHave = AccountSystem.s_Instance.GetGreenCash();
                    nPrice = m_nPrice_Diamond;
                    szMoneyType = "钻石";
                }
                break;

            case ShoppinMall.ePriceType.coin:
                {
                    nCurHave = AccountSystem.s_Instance.GetCoin( (int)m_ePriceSubType );
                    nPrice = m_nPrice_Coin;
                    szMoneyType = "金币";
                }
                break;

        } // end switch


        if (nCurHave < nPrice)
        {
            UIMsgBox.s_Instance.ShowMsg("相应货币不够");
            return;
        }


        ShoppinMall.s_Instance._subpanelConfirmBuy.SetActive(true);
        ShoppinMall.s_Instance._CurProcessingBuyUiItem = this;
        ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_ePriceType = ePriceType;

        string szDesc = "";
         
        ShoppinMall.s_Instance._txtConfirmBuy_Cost.text = "花费 " + szMoneyType + "<Color=#F9E15A>" + nPrice/*ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nPrice*/ + "</Color> 购买？";
       //ShoppinMall.s_Instance._txtConfirmBuy_Value.text = "X" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nValue0;
        if (m_eItemType == ShoppinMall.eItemType.coin_raise)
        {
            ShoppinMall.s_Instance._imgConfirmBuy_Avatar.sprite = ResourceManager.s_Instance.m_aryCoinRaiseItemIcon[ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nItemId];
            szDesc = "所有星球在" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nDuration + "秒内收入X" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nValue0;
        }
        else if (m_eItemType == ShoppinMall.eItemType.skill_point)
        {
            ShoppinMall.s_Instance._imgConfirmBuy_Avatar.sprite = ResourceManager.s_Instance.m_arySkillPointIcon[m_nItemId];
            string szPlanetName = "";
            switch(m_nItemId)
            {
                case 0:
                    {
                        szPlanetName = "青铜星";
                    }
                    break;
                case 1:
                    {
                        szPlanetName = "白银星";
                    }
                    break;
                case 2:
                    {
                        szPlanetName = "黄金星";
                    }
                    break;
            }
            szDesc = "获得 " + szPlanetName + " 的技能点";
        }
        ShoppinMall.s_Instance._txtConfirmBuy_FuncAndValue.text = szDesc;


    } // end DoBuy

    public void OnClickButton_Buy()
    {
        if (m_eItemType == ShoppinMall.eItemType.diamond)
        {
            UIMsgBox.s_Instance.ShowMsg("此功能暂未开放");
            return;
        }

        double nCurHave = 0;
        double nPrice = 0;
        switch(m_ePriceType)
        {
            case ShoppinMall.ePriceType.green_cash:
                {
                    nCurHave = AccountSystem.s_Instance.GetGreenCash();
                    nPrice = m_nPrice_Diamond;
                }
                break;

            
        } // end switch



        if (nCurHave < nPrice)
        {
            UIMsgBox.s_Instance.ShowMsg( "钱不够" );
            return;
        }

        ShoppinMall.s_Instance._subpanelConfirmBuy.SetActive( true );
        ShoppinMall.s_Instance._CurProcessingBuyUiItem = this;
   

        string szDesc = "";
        ShoppinMall.s_Instance._txtConfirmBuy_Cost.text = "花费 现金 <Color=#F9E15A>" + nPrice/*ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nPrice*/ + "</Color> 购买？";
        ShoppinMall.s_Instance._txtConfirmBuy_Value.text = "X" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nValue0;
        if (m_eItemType == ShoppinMall.eItemType.coin_raise)
        {
            ShoppinMall.s_Instance._imgConfirmBuy_Avatar.sprite = ResourceManager.s_Instance.m_aryCoinRaiseItemIcon[ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nItemId];
            szDesc = "所有星球在" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nDuration + "秒内收入X" + ShoppinMall.s_Instance._CurProcessingBuyUiItem.m_nValue0;
        }
        else if (m_eItemType == ShoppinMall.eItemType.skill_point)
        {
            ShoppinMall.s_Instance._imgConfirmBuy_Avatar.sprite = ResourceManager.s_Instance.m_arySkillPointIcon[m_nItemId];
            szDesc = "获得" + (m_nItemId) + "号星球的技能点";
        }
        ShoppinMall.s_Instance._txtConfirmBuy_FuncAndValue.text = szDesc;

    }

    public void InitBagItem( UIItem buy_item )
    {
        m_bInBag = true;
        m_nItemId = buy_item.m_nItemId;
        m_nValue0 = buy_item.m_nValue0;
        m_nDuration = buy_item.m_nDuration;
        _txtDesc.text = buy_item._txtDesc.text;
        _txtValue.text = "使用";
        _txtName.text = buy_item._txtName.text;

        SetCountingPanelVisible( false );
        SetUseButtonVisible( true );
    }



    public void InitBagItemById( int nItemId )
    {
        m_bInBag = true;
        m_nItemId = nItemId;

        int nValue0 = 0;
        int nDuration = 0;
        string szDesc = "";
        string szName = "";
        switch( nItemId )
        {
            case 1:
                {
                    nValue0 = 2;
                    nDuration = 30;
                    szDesc = "30秒";
                    szName = "2倍";
                }
                break;
        } // end switch


        m_nValue0 = nValue0;
        m_nDuration = nDuration;
        _txtDesc.text = szDesc;
        _txtValue.text = "使用";
        _txtName.text = szName;

        SetCountingPanelVisible(false);
        SetUseButtonVisible(true);
    }

    public void OnClickButton_Use()
    {
         ItemSystem.s_Instance.UseItem( this );



    }

    public void SetUseEnalbed( bool bEnabled )
    {
       _btnUse.enabled = bEnabled;
    }

    public void SetCountingPanelVisible( bool bVisible )
    {
      // _panelCounting.SetActive(bVisible);
    }

    public void SetUseButtonVisible( bool bVisible )
    {
       // _btnUse.gameObject.SetActive( bVisible );
    }

    public bool DoCount()
    {
        float fTimeElapse = Main.GetTime() - m_fStartTime;
        float fTimeLeft = m_nDuration - fTimeElapse;
        _txtLeftTime.text = fTimeLeft.ToString( "f0" );

        if (fTimeLeft <= 0)
        {
            return true;
        }

        return false;
    }

    public void SetBagItemConfig( ItemSystem.sItemConfig config )
    {
        m_BagItemConfig = config;

        _txtName.text = m_BagItemConfig.szName;
    }

    public int GetBagItemId()
    {
        return m_BagItemConfig.nId;
    }

    public void SetNum( int nNum )
    {
        _txtNum.text = nNum.ToString();
        m_nNum = nNum;
    }

    public int GetNum()
    {
        return m_nNum;
    }

    public void SetIntParam( int nIndex, int nValue )
    {
        m_aryIntParams[nIndex] = nValue;
    }

    public int GetIntParam(int nIndex )
    {
        return m_aryIntParams[nIndex];
    }

    public void SetFloatParam(int nIndex, float fValue )
    {
        m_aryFloatParams[nIndex] = fValue;
    }

    public float GetFloatParam(int nIndex)
    {
        return m_aryFloatParams[nIndex];
    }

    public void SetLeftTime( int nLeftTime )
    {
        m_nLeftTime = nLeftTime;
        _txtLeftTime.text = m_nLeftTime.ToString();
    }

    public int GetLeftTime()
    {
        return m_nLeftTime;
    }

    public int GetId()
    {
        return m_Config.nId;
    }

    public bool Counting()
    {
        int nLeftTime = GetLeftTime();
        nLeftTime -= 1;
        SetLeftTime(nLeftTime);



        return nLeftTime <= 0;
    }

    public UIItem Clone()
    {
        UIItem clone_item = ShoppinMall.s_Instance.NewBuyCounter();
        clone_item.SetConfig( this.m_Config );

        return clone_item;
    }

} // end class
