﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDistrict : MonoBehaviour {

    public GameObject _lock;
    public bool m_bLocked = true;
    public bool m_bCanUnlock = false;

    public District m_District;

    public GameObject _goWenHao;

    /// <summary>
    /// / UI
    /// </summary>
    public Text _txtUnlockPrice ;
    public Text _txtTrackName; //  赛道的名字
    public GameObject _goPrice;
    public Image _imgMoneyIcon; // “解锁价格”的金币图标
    public Image _imgMoneyIcon_ShouYi; // “收益”的金币图标
    public Text _txtTiSheng;
    public Text _txtShouYi;
    public GameObject _contaainerShouYi;
    public GameObject _containerTiShengAndShouYi;

    public Image _imgBg;

    public Text _txtIsCurDistrict;

    /// end UI
   

    public Button _btnEnter;

    public float m_fEnterButtonShowTime = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ShowButtonEnterLoop();

    }

    public void SetDistrict( District district )
    {
        m_District = district;
        DataManager.sTrackConfig config = DataManager.s_Instance.GetTrackConfigById(district.GetBoundPlanet().GetId(), district.GetId());
        SetUnlockPrice(config.nUnlockPrice  /*district.GetUnlockPrice()*/ );
        _txtTrackName.text = config.szName + " Lv." + ( config.nId + 1 );


    }

    public void SetLocked( bool bLocked, int nCounterIndex )
    {
        //_lock.SetActive( bLocked);

        _goPrice.SetActive(bLocked);
        _txtUnlockPrice.gameObject.SetActive(bLocked);
        _containerTiShengAndShouYi.SetActive( !bLocked);

        bool bLeft = nCounterIndex % 2 != 0;
        if ( bLocked )
        {
            if ( bLeft )
            {
                _imgBg.sprite = MapManager.s_Instance.m_sprBg_Locked_Left;
            }
            else
            {
                _imgBg.sprite = MapManager.s_Instance.m_sprBg_Locked_Right;
            }
        }
        else
        {
            if (bLeft)
            {
                _imgBg.sprite = MapManager.s_Instance.m_sprBgUnlocked_Left;
            }
            else
            {
                _imgBg.sprite = MapManager.s_Instance.m_sprBgUnlocked_Right;
            }
        }
    }

    public void OnClickMe()
    {
        if ( m_bLocked )
        {
            if (m_bCanUnlock)
            {
                MapManager.s_Instance.PreUnLockDistrict(m_District);
            }
            else
            {
                UIMsgBox.s_Instance.ShowMsg( "请先解锁上一级赛道" );
            }
        }
        else
        {
            //BeginShowButtonEnter();
            // 直接进入该赛道
            OnClickEnterRace();
        }
    }

    void BeginShowButtonEnter()
    {
        m_fEnterButtonShowTime = 2f;
        _btnEnter.gameObject.SetActive(true);
    }

    void ShowButtonEnterLoop()
    {
        if (m_fEnterButtonShowTime <= 0)
        {
            return;
        }

        m_fEnterButtonShowTime -= Time.deltaTime;
        if (m_fEnterButtonShowTime <= 0)
        {
            _btnEnter.gameObject.SetActive( false );
        }
    }

    public void SetCanUnlock( bool bCan )
    {
        m_bCanUnlock = bCan;

        _goPrice.SetActive(m_bCanUnlock);
    }

    public void SetUnlockPrice( double nPrice )
    {
        _txtUnlockPrice.text = CyberTreeMath.GetFormatMoney(nPrice) ;//nPrice.ToString("f0");
    }

    public void OnClickEnterRace()
    {
     //   Debug.Log( "enter: " + MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI+ "," + m_District.GetId() );
        MapManager.s_Instance.PreEnterRace(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI, m_District.GetId());
    }

} // end class
