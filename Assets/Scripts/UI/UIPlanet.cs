﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlanet : MonoBehaviour {

    public enum eUIPlanetType
    {
        main,      // 主行星
        adventrue, // 探险星球
        research,  // 研究院

        others,    // ??
    };

    public GameObject _lock;

    public bool m_bLocked = true;
    public int m_nPlanetId = 0;

    public eUIPlanetType m_ePlanetType;

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtUnlockPrice;// 星球的解锁价格
    public Text _txtPlanetName; // 星球的名字
    public GameObject _goLock;
    public GameObject _goPrice;
    public GameObject _goButtonMain;
    // end UI

    public BaseRotate _BaseRotate;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickMe()
    {
        switch( m_ePlanetType )
        {
            case eUIPlanetType.main:
                {
                    OnClickMain();
                }
                break;
        } // end switch
    }

    public void OnClickMain()
    {
        if ( m_bLocked )
        {
            //MapManager.s_Instance.UnLockPlanet(m_nPlanetId);
            MapManager.s_Instance.PreUnlockPlanet(m_nPlanetId);
        }
        else
        {
            MapManager.s_Instance.OpenPlanetDetailPanel( m_nPlanetId );
        }
    }

    public void SetUnlockPrice( double nPrice )
    {
        _txtUnlockPrice.text = CyberTreeMath.GetFormatMoney(nPrice) ; // nPrice.ToString( "f0" );
    }

    public void SetUnlock(bool bUnlock)
    {
        _goLock.SetActive(!bUnlock);
        _goPrice.SetActive(!bUnlock);
        m_bLocked = !bUnlock;

        if (_BaseRotate)
        {
            _BaseRotate.m_bRotating = bUnlock;
        }
    }







} // end class
