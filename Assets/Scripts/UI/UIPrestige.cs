﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPrestige : MonoBehaviour {

    public Text _txtCurGainTimes;
    public Text _txtNextGainTimes;
    public Text _txtCost;
    public Button _btnDoPrestige;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void UpdateInfo( int nPlanetId = -1, int nDistrictId = -1 )
    {
        Planet planet = null;
        District district = null;
        if (nPlanetId == -1 || nDistrictId == -1)
        {
            nPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
            nDistrictId = MapManager.s_Instance.GetCurDistrict().GetId();
        }
   
        planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        district = planet.GetDistrictById(nDistrictId);


        MapManager.s_Instance.m_PlanetToPrestige = planet;
        MapManager.s_Instance.m_DistrictToPrestige = district;

        int nCurPrestigeTimes = district.GetPrestigeTimes();
        int nCurGainTimes = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nCurPrestigeTimes);
        int nNextGainTimes = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nCurPrestigeTimes + 1);
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(nPlanetId, nDistrictId);

        _txtCost.text = nCost.ToString();
        _txtCurGainTimes.text = nCurGainTimes.ToString() + "X";
        _txtNextGainTimes.text = nNextGainTimes.ToString() + "X";

    }



} // end class
