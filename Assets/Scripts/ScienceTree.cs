﻿/*
 * 科技树
 * 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScienceTree : MonoBehaviour
{

    public static ScienceTree s_Instance = null;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempPos1 = new Vector3();

    public const int MAX_LEAF_NUM_OF_BRANCH = 36;
    public const int MAX_LEVEL_OF_LEAF = 30;

    public enum eBranchType
    {
        branch0,
        branch1,
        branch2,
    };

    public enum eScienceType
    {
        coin_raise,
        cost_reduce,
        ads_duration,
        ads_coin_raise,
        speed_accelerate,

        skill_speed_accelerate_time_raise,
        skill_coin_raise_raise,
    };

    Dictionary<eBranchType, int> m_dicSkillPoint = new Dictionary<eBranchType, int>();

    public ScienceLeaf[] aryBranch0;
    public ScienceLeaf[] aryBranch1;
    public ScienceLeaf[] aryBranch2;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _panelScienceTree;

    // sub-panel 
    public GameObject _containerTree; // 这个容易用于整体拖动
    public GameObject _subpanelUpgrade;
    public Text _txtLevel;
    public GameObject _containerThisLevelInfo;
    public GameObject _containerNextLevelInfo;

    public Button _btnUpgrade;
    public Text _txtPrice;
    public Image _imgMoneyIcon;
    public Text _txtOperateType;

    public GameObject _containerLevel;

    public Text _txtCurLevelValue;
    public Text _txtNextLevelValue;
    public Text _txtDesc;

    public MoneyCounter _moneyBranch0;
    public MoneyCounter _moneyBranch1;
    public MoneyCounter _moneyBranch2;

    // end UI

    Dictionary<eBranchType, ScienceLeaf[]> m_dicTree = new Dictionary<eBranchType, ScienceLeaf[]>();
    Dictionary<eBranchType, sLeafConfig[]> m_dicLeafConfig = new Dictionary<eBranchType, sLeafConfig[]>();

    public ScienceLeaf m_CurSelectedLeaf = null;




    public enum eLeafConfig
    {
        coin_raise, // 金币增益提升
    };

    public struct sLeafConfig
    {
        public eLeafConfig eType;
        public int[] aryIntParams;
        public float[] aryFloatParams;
        public string szDesc;
        public bool bSwitch;
    };
    sLeafConfig tempLeafConfig;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        LoadConfig();

        // poppin test
        SetSkillPoint(eBranchType.branch0, 1000);
        SetSkillPoint(eBranchType.branch1, 1000);
        SetSkillPoint(eBranchType.branch2, 1000);
    }

    // Update is called once per frame
    void Update()
    {

        ProcessDragTree();

    }

    // 拖动整个树(因为一屏显示不下)
    Vector3 m_vecDragLastPos = new Vector3();
    bool m_bDragging = false;
    void ProcessDragTree()
    {
        /*
        if (Input.touchCount == 0)
        {
            m_bDragging = false;
            Main.s_Instance.SetDebugInfo( "no touch" );
            return;
        }

        Touch touch = Input.GetTouch(0);

      
        if (!m_bDragging)
        {
            m_vecDragLastPos = touch.position;
        }
        m_bDragging = true;

            vecTempPos = (touch.position - m_vecDragLastPos );
        Main.s_Instance.SetDebugInfo(touch.position  + "," + m_vecDragLastPos + "_" + _containerTree.transform.localPosition);
        vecTempPos1 = _containerTree.transform.localPosition;
            vecTempPos1.x += vecTempPos.x;
            vecTempPos1.y += vecTempPos.y;
            _containerTree.transform.localPosition = vecTempPos1;

        m_vecDragLastPos = touch.position;
        */
        if ( Input.GetMouseButton( 0 ) )
        {
         

            if (!m_bDragging)
            {
                m_vecDragLastPos = Input.mousePosition;
            }
            m_bDragging = true;

            vecTempPos = (Input.mousePosition - m_vecDragLastPos);

            vecTempPos1 = _containerTree.transform.localPosition;
            vecTempPos1.x += vecTempPos.x;
            vecTempPos1.y += vecTempPos.y;
            _containerTree.transform.localPosition = vecTempPos1;

            m_vecDragLastPos = Input.mousePosition;
        }
        else
        {

            m_bDragging = false;
        }



    }

    public void GetSkillConfig( SkillManager.eSkillType eType, ref SkillManager.sSkillConfig config )
    {
        switch(eType)
        {
            case SkillManager.eSkillType.speed_accelerate:
                {
                    config.fValue = 3f;
                    config.nColdDown = 10;
                    config.nDuration = 30;
                }
                break;
            case SkillManager.eSkillType.coin_raise:
                {
                    config.fValue = 2f;
                    config.nColdDown = 10;
                    config.nDuration = 30;
                }
                break;
            case SkillManager.eSkillType.cost_reduce:
                {
                    config.fValue = -0.4f;
                    config.nColdDown = 10;
                    config.nDuration = 30;
                }
                break;
        } // end switch

    }

    public void OpenScienceTree()
    {
        _panelScienceTree.SetActive( true );
    }

    public void CloseScienceTree()
    {
        _panelScienceTree.SetActive(false);
    }

    public void OpenSubPanelUpgrade()
    {
        _subpanelUpgrade.SetActive( true );
    }

    public void CloseSubPanelUpgrade()
    {
        _subpanelUpgrade.SetActive(false);
    }


    Dictionary<int, int> m_dicPointsForLevels = new Dictionary<int, int>();

    IEnumerator LoadConfig_TalentGeneral(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载
        string[] aryLines = www.text.Split('\n');
        string[] aryPointForLevels = aryLines[2].Split(',');

        // 提升等级所需的技能点
        for (int i = 0; i < aryPointForLevels.Length; i++ )
        {
            int nPoint = 0;
            if ( int.TryParse(aryPointForLevels[i], out nPoint) )
            {
                m_dicPointsForLevels[i] = nPoint;
            }
        }

        ///// 天赋路线1的数值配置
        string[] aryGeneralParams = aryLines[5].Split(',');
        string[] aryPlanetAndTrackInfo = aryLines[6].Split(',');
        sLeafConfig[] aryLeafConfig = new sLeafConfig[MAX_LEAF_NUM_OF_BRANCH];
        float fSingleTrackPromote = float.Parse(aryGeneralParams[0]);
        float fSingleTrackPromoteStep = float.Parse(aryGeneralParams[1]);
        string szSingleTrackDesc = aryGeneralParams[2];
        float fAllTrackPromote = float.Parse(aryGeneralParams[3]);
        float fAllTrackPromoteStep = float.Parse(aryGeneralParams[4]);
        string szAllTrackDesc = aryGeneralParams[5];

        for (int i = 0; i < aryPlanetAndTrackInfo.Length; i++ )
        {
            sLeafConfig config = new sLeafConfig();
            config.aryIntParams = new int[8];
            config.aryFloatParams = new float[8];
            if (aryPlanetAndTrackInfo[i] == "")
            {
                continue;
            }
            string[] aryParams = aryPlanetAndTrackInfo[i].Split( '_' );
            if (aryParams.Length < 2)
            {
                continue;
            }
            if (aryParams[0] == "-1" || aryParams[1] == "-1") // all planets and all trackss
            {
                config.bSwitch = true; // all
                config.aryIntParams[0] = int.Parse(aryParams[0]);
                config.aryIntParams[1] = int.Parse(aryParams[1]);
                config.aryFloatParams[0] = fAllTrackPromote;
                config.aryFloatParams[1] = fAllTrackPromoteStep;
                // config.szDesc = string.Format(szAllTrackDesc, (config.aryFloatParams[0] * 100).ToString("f0"));
                config.szDesc = szAllTrackDesc;
            }
            else
            {
                config.aryIntParams[0] = int.Parse(aryParams[0]);
                config.aryIntParams[1] = int.Parse(aryParams[1]);
                config.bSwitch = false;
                config.aryFloatParams[0] = fSingleTrackPromote;
                config.aryFloatParams[1] = fSingleTrackPromoteStep;
                // config.szDesc = string.Format(szSingleTrackDesc, config.aryIntParams[0], config.aryIntParams[1], config.aryFloatParams[0] * 100);
                config.szDesc = szSingleTrackDesc;
            }

         
         
            aryLeafConfig[i] = config;
        }

        m_dicLeafConfig[eBranchType.branch0] = aryLeafConfig;

        //// end 天赋路线1的数值配置

        /*
        // 购买技能点所需的价格 
        for (int i = 4; i <= 6; i++ )
        {
            string[] aryParams= aryLines[i].Split(',');
            sTalentPointBuyConfig config = new sTalentPointBuyConfig();
            int nColIndex = 1;
            config.nCointStartPrice = double.Parse(aryParams[nColIndex++]);
            config.fRisePercentAfterBuy = float.Parse(aryParams[nColIndex++]);
            config.nDiamondPrice = int.Parse(aryParams[nColIndex++]);
            int nKey = i - 4;
            m_dicTalentPointBuyConfig[nKey] = config;
        } // end for (int i = 4; i <= 6; i++ )
        */



    }// end LoadConfig_TalentGeneral

    public sLeafConfig GetLeafConfig(eBranchType eType, int nLeafIndex )
    {
        sLeafConfig[] aryConfig = null;
        if ( !m_dicLeafConfig.TryGetValue( eType, out aryConfig ) )
        {
            Debug.LogError( "error" );
        }
        tempLeafConfig = aryConfig[nLeafIndex];
        return tempLeafConfig;
    }


    // 每级需要消耗多少天赋点。0级就是解锁操作
    public int GetPointForLevel( int nLevel )
    {
        int nPoint = 0;
        if ( !m_dicPointsForLevels.TryGetValue( nLevel, out nPoint ) )
        {
            Debug.LogError( "error" );
            nPoint = 0;
        }
        return nPoint;
    }


    public void LoadConfig()
    {
        string szConfigFileName_TalentGeneral = DataManager.url + "talent_general.csv";
        StartCoroutine(LoadConfig_TalentGeneral(szConfigFileName_TalentGeneral));

        m_dicTree[eBranchType.branch0] = aryBranch0;
        m_dicTree[eBranchType.branch1] = aryBranch1;
        m_dicTree[eBranchType.branch2] = aryBranch2;

        for (int i = 0; i < MAX_LEAF_NUM_OF_BRANCH; i++ ) 
        {
            foreach( KeyValuePair<eBranchType, ScienceLeaf[]> pair in m_dicTree )
            {
                ScienceLeaf[] aryBranch = pair.Value;
                if ( i < aryBranch.Length)
                {
                    ScienceLeaf leaf = aryBranch[i];
                    if ( leaf == null )
                    {
                        continue;
                    }
                    leaf.SetIndex(i);
                    leaf.SetType(pair.Key);
                }
            }
        }


        return;


        // leaf 0
        for (int i = 0; i < 5; i++ )
        {
            if ( i >= aryBranch0.Length)
            {
                break;
            }

            ScienceLeaf leaf = aryBranch0[i];

            if ( leaf == null )
            {
                continue;
            }

            ScienceTreeConfig config = ResourceManager.s_Instance.NewTreeConfig();

            config.szDesc = "青铜星" + "赛道" + (i + 1) + "金币增益";
            config.m_szKey = "0_" + i;
            config.m_eScienceType = eScienceType.coin_raise;

            for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
            {
                int nLevel = j + 1;
                config.aryPrice[j] = nLevel;
                config.aryValue[j] = nLevel + 1;
            } // end for

            leaf.m_Config = config;
            aryBranch0[i] = leaf;

            // end leaf 0
        } // end for i



        //// end branch 0


        //// ------------------- branch 1 -----------------
        int nPlanetIndex = 0;
        for (int i = 0; i < 5; i += 2)
        {
            ScienceLeaf leaf = aryBranch1[i];

            if (leaf == null)
            {
                continue;
            }

            ScienceTreeConfig config = ResourceManager.s_Instance.NewTreeConfig();
           
          // config.szDesc = "星球" + nPlanetIndex + "交通工具打折";
                      config.szDesc = MapManager.s_Instance.GetPlanetNameById(nPlanetIndex) + "交通工具打折";
            config.m_szKey = nPlanetIndex.ToString();
            config.m_eScienceType = eScienceType.cost_reduce;

            for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
            {
                int nLevel = j + 1;
                config.aryPrice[j] = nLevel;
                config.aryValue[j] = -0.3f - j * 0.02f;
                if (config.aryValue[j] < -0.8f)
                {
                    config.aryValue[j] = -0.8f;
                }
            } // end for

            leaf.m_Config = config;
            aryBranch1[i] = leaf;


            nPlanetIndex++;
        } // end for i

        // 广告持续时间
        ScienceLeaf leaf_1_1 = aryBranch1[1];
        ScienceTreeConfig config_1_1 = ResourceManager.s_Instance.NewTreeConfig();
        config_1_1.szDesc = "广告提升持续时间提高";
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_1_1.aryPrice[j] = nLevel;
            config_1_1.aryValue[j] = 0.15f * nLevel;
           
        } // end for j
        leaf_1_1.m_Config = config_1_1;

        // end 广告持续时间

        // 广告金币加成
        ScienceLeaf leaf_1_3 = aryBranch1[3];
        ScienceTreeConfig config_1_3 = ResourceManager.s_Instance.NewTreeConfig();
        config_1_3.szDesc = "广告金币加成提高";
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_1_3.aryPrice[j] = nLevel;
            config_1_3.aryValue[j] = 0.2f * nLevel;

        } // end for j
        leaf_1_3.m_Config = config_1_3;

        // end 广告金币加成


        /// ----------------- end branch 1 ---------------


        //// ------------------ branch 2 ---------------------
        nPlanetIndex = 0;
        for (int i = 0; i < 5; i += 2)
        {
            ScienceLeaf leaf = aryBranch2[i];

            if (leaf == null)
            {
                continue;
            }

            ScienceTreeConfig config = ResourceManager.s_Instance.NewTreeConfig();

            config.szDesc = MapManager.s_Instance.GetPlanetNameById(nPlanetIndex) + "交通工具加速";
            config.m_szKey = nPlanetIndex.ToString();
            config.m_eScienceType = eScienceType.speed_accelerate;

            for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
            {
                int nLevel = j + 1;
                config.aryPrice[j] = nLevel;
                config.aryValue[j] = 0.2f * nLevel;
               
            } // end for

            leaf.m_Config = config;


            nPlanetIndex++;
        } // end for i

        ScienceLeaf leaf_2_1 = aryBranch2[1];
        ScienceTreeConfig config_2_1 = ResourceManager.s_Instance.NewTreeConfig();
        config_2_1.szDesc = "加速技能持续时间提高";
        config_2_1.m_eScienceType = eScienceType.skill_speed_accelerate_time_raise;
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_2_1.aryPrice[j] = nLevel;
            config_2_1.aryValue[j] = 0.2f * nLevel;

        } // end for

        leaf_2_1.m_Config = config_2_1;
        // end 


        ScienceLeaf leaf_2_3 = aryBranch2[3];
        ScienceTreeConfig config_2_3 = ResourceManager.s_Instance.NewTreeConfig();
        config_2_3.szDesc = "金币增益技能提高";
        config_2_3.m_eScienceType = eScienceType.skill_coin_raise_raise;
        for (int j = 0; j < MAX_LEVEL_OF_LEAF; j++)
        {
            int nLevel = j + 1;
            config_2_3.aryPrice[j] = nLevel;
            config_2_3.aryValue[j] = 0.2f * nLevel;

        } // end for

        leaf_2_3.m_Config = config_2_3;

        /// ----------- end branch 2 --------------


        m_dicTree[eBranchType.branch0] = aryBranch0;
        m_dicTree[eBranchType.branch1] = aryBranch1;
        m_dicTree[eBranchType.branch2] = aryBranch2;
        //// branch 1

    }

    public float GetSkillCoinRaiseRaise()
    {
        return aryBranch2[3].GetCurValue();
    }

    public float GetSkillSpeedTimeRaise()
    {
        return aryBranch2[1].GetCurValue();
    }

    public float GetAccelerateRaise( int nPlanetId )
    {
        switch(nPlanetId)
        {
            case 0:
                {
                    return aryBranch2[0].GetCurValue();
                }
                break;
            case 1:
                {
                    return aryBranch2[2].GetCurValue();
                }
                break;
            case 2:
                {
                    return aryBranch2[4].GetCurValue();
                }
                break;
        } // end switch

        return 0;
    }

    // 获取广告加成值
    public float GetAdsCoinRaise()
    {

        ScienceLeaf leaf = aryBranch1[3];
        return leaf.GetCurValue();
    }

    // 获取广告时间提升值
    public float GetAdsTimeRaise()
    {

        ScienceLeaf leaf = aryBranch1[1];
        return leaf.GetCurValue();
    }


    //  获取交通工具购买折扣
    public float GetVehicleBuyDiscount(int nPlanetId )
    {
        float fDiscount = 0f;
        string szKey = nPlanetId.ToString();
        ScienceLeaf[] aryBranch1 = m_dicTree[eBranchType.branch1];
        for (int i = 0; i < aryBranch1.Length; i++)
        {
            ScienceLeaf leaf = aryBranch1[i];
            if (leaf == null)
            {
                return 0;
            }
            if (leaf.m_Config.m_szKey == szKey)
            {
                return leaf.GetCurValue();
            }
        }

        return fDiscount;
    }

    //  获取金币加成的数值
    public float GetCoinRaise( int nPlanetId, int nDistrictId )
    {
        float fRaise = 0f;

        string szKey = nPlanetId + "_" + nDistrictId;
        ScienceLeaf[] aryBranch0 = m_dicTree[eBranchType.branch0];
        for (int i = 0; i < aryBranch0.Length; i++ )
        {
            ScienceLeaf leaf = aryBranch0[i];
            if ( leaf == null )
            {
                return 0;
            }
            if ( leaf.m_Config.m_szKey == szKey )
            {
                return leaf.GetCurValue();
            }
        }

        return fRaise;
    }

    public void SetValueUiContent( eScienceType eType, Text txtValue, float fValue )
    {
        switch(eType)
        {
            case eScienceType.cost_reduce:
                {
                    txtValue.text = ( fValue * 100 )+ "%";
                }
                break;

            default:
                {
                    txtValue.text = fValue.ToString();
                }
                break;
        } // end switch

    }

    public void UpdateSubPanelInfo(ScienceLeaf leaf)
    {
        /*
        if ( leaf.m_Config == null )
        {
            return;
        }
        */

        _imgMoneyIcon.sprite = ResourceManager.s_Instance.m_arySkillPointIcon[(int)leaf.m_eType];

        if (leaf.IsUnlocked()) // 该节点已解锁
        {
            _containerLevel.SetActive(true);
            _txtLevel.text = leaf.GetLevel().ToString();

            _txtOperateType.gameObject.SetActive(true);
            _txtOperateType.text = "升级";

            _containerNextLevelInfo.SetActive( true );

            _btnUpgrade.gameObject.SetActive(true);

            /*
            SetValueUiContent(leaf.GetScienceType(), _txtCurLevelValue, leaf.GetCurValue());
            SetValueUiContent(leaf.GetScienceType(), _txtNextLevelValue, leaf.GetNextValue());
            */
        }
        else // 该节点未解锁
        {
            _containerLevel.SetActive(false);
            bool bCanUnlock = false;
            // 判断该节点能否解锁：如果它的上一个节点还没解锁，则它不能解锁
            if (leaf.GetIndex() == 0) // 它是这条支线的第一个节点，可以解锁
            {
                bCanUnlock = true;
            }
            else
            {
                // 取它的上一个节点，看看解锁没有
                ScienceLeaf[] branch = m_dicTree[leaf.GetType()];
                int nIndexPrevLeaf = leaf.GetIndex() - 1;
                ScienceLeaf prev_leaf = branch[nIndexPrevLeaf];
                if (prev_leaf.IsUnlocked()) // 它的上一个节点已解锁
                {
                    bCanUnlock = true;
                }
            }


            //    _containerLevel.gameObject.SetActive(false);
            _txtLevel.text = "0";
          // _containerNextLevelInfo.SetActive(false);
            if (bCanUnlock) // 可以解锁
            {
                _txtOperateType.text = "解锁";
                _txtOperateType.gameObject.SetActive( true );
                _btnUpgrade.gameObject.SetActive(true);
            }
            else // 不能解锁
            {
                _txtOperateType.text = "先解锁上个节点";
                //_txtOperateType.gameObject.SetActive(false);
                _btnUpgrade.gameObject.SetActive( false );
            }



            /*
            SetValueUiContent(leaf.GetScienceType(), _txtCurLevelValue, 0);

            // _txtCurLevelValue.text = leaf.GetNextValue().ToString();
            SetValueUiContent(leaf.GetScienceType(), _txtNextLevelValue, leaf.GetNextValue());
*/



        } // end 该节点未解锁

        sLeafConfig config = GetLeafConfig(leaf.GetType(), leaf.GetIndex());
        if ( leaf.IsUnlocked() ) // 尚未解锁
        {
           
        }
        else // 已解锁
        {

        }
        //_txtDesc.text = config.szDesc;

        // 解锁或升级价格
        int nCurLevel = leaf.GetLevel();
        bool bNextLevelExist = false;
        int nPoint = GetPointForLevel(nCurLevel);
        if ( nPoint > 0 )
        {
            bNextLevelExist = true;
        }
        _txtPrice.text = nPoint.ToString();

        // 下一级的配置
   
       
        switch (m_CurSelectedLeaf.GetType())
        {
            case eBranchType.branch0:
                {
                    Main.s_Instance.UpdateRaise();

                    // (注：此处跟具体的逻辑玩法紧密相关，代码无法做到有通用性，只有见招拆招)
                    // 更新一下本节点的参数
             
                    int nNewLevel = m_CurSelectedLeaf.GetLevel();
                    float fCoinPromote = config.aryFloatParams[0];
                    float fCoinPromoteNextLevel = 0;
                    int nPlanetId = config.aryIntParams[0];
                    int nTrackId = config.aryIntParams[1];
                    for (int i = 0; i < nNewLevel - 1; i++)
                    {
                        fCoinPromote *= (1f + config.aryFloatParams[1]);
                    }
                    fCoinPromoteNextLevel = fCoinPromote * (1f + config.aryFloatParams[1]);

                    m_CurSelectedLeaf.SetSwitch(config.bSwitch);
                    m_CurSelectedLeaf.SetFloatParam(0, fCoinPromote);
                    m_CurSelectedLeaf.SetIntParam(0, nPlanetId);
                    m_CurSelectedLeaf.SetIntParam(1, nTrackId);
                    m_CurSelectedLeaf.SetFloatParam(0, fCoinPromote);

                    // 更新描述信息 
                    if (config.bSwitch)
                    {
                        _txtDesc.text = string.Format(config.szDesc, (fCoinPromote * 100).ToString("f2"));
                    }
                    else
                    {
                        _txtDesc.text = string.Format(config.szDesc, nPlanetId, nTrackId, (fCoinPromote * 100).ToString("f2"));

                    }


                 
                    // 存在下一级配置
                    if (bNextLevelExist)
                    {
                        _txtNextLevelValue.text = (fCoinPromoteNextLevel * 100).ToString("f2") + "%";
                    }
                    else
                    {
                        _txtNextLevelValue.text = "（已满级)";
                        _btnUpgrade.gameObject.SetActive(false);
                    }
                
                }
                break;


        } // end switch


        Main.s_Instance.UpdateRaise();
    }

    public float GetCoinPromote( int nPlanetId, int nTrackId, ref string szTalentCoinPromoteInfo)
    {
        float fCoinPromote = 0f;
        szTalentCoinPromoteInfo = "";

        ScienceLeaf[] aryLeafs = m_dicTree[eBranchType.branch0];
        bool bFirst = true;
        for (int i = 0; i < aryLeafs.Length; i++ )
        {
            ScienceLeaf leaf = aryLeafs[i];
            if ( !leaf.IsUnlocked() )
            {
                break;
            }

            if (bFirst)
            {
                szTalentCoinPromoteInfo += "+";
            }
            else
            {
                szTalentCoinPromoteInfo += " ";
            }
            bFirst = false;
            float fPromoteOfThisLeaf = 0;
            if ( leaf.GetIntParam(0) == nPlanetId && leaf.GetIntParam(1) == nTrackId) // 本赛道有加成
            {
                fPromoteOfThisLeaf = 1f + leaf.GetFloatParam(0);
                fCoinPromote += fPromoteOfThisLeaf;
 
                szTalentCoinPromoteInfo += ( leaf.GetIndex() + "号天赋节点效果：星球" + nPlanetId + "赛道" + nTrackId + "加成 X " + fPromoteOfThisLeaf) + "\n";

            }
            else if (leaf.GetIntParam(1) == -1) // 所有赛道有加成
            {
                fPromoteOfThisLeaf = 1f + leaf.GetFloatParam(0);
                fCoinPromote += fPromoteOfThisLeaf;
                szTalentCoinPromoteInfo += (leaf.GetIndex() + "号天赋节点效果：所有星球所有赛道加成 X " + fPromoteOfThisLeaf) + "\n";

            }
        }
        szTalentCoinPromoteInfo += "=" + fCoinPromote;


        return fCoinPromote;
    }

    public void ProcessLeaf( ScienceLeaf leaf )
    {
        m_CurSelectedLeaf = leaf;

        _subpanelUpgrade.SetActive( true );


        UpdateSubPanelInfo(m_CurSelectedLeaf);
    }

    public void OnClick_UnlockOrUpgrade()
    {
        eBranchType eType = m_CurSelectedLeaf.m_eType;
        int nLeafIndex = m_CurSelectedLeaf.GetIndex();
        int nLevel = m_CurSelectedLeaf.GetLevel();
        int nPoint = GetSkillPoint(eType);

        bool bNextLevelExist = false;
        int nToCost = GetPointForLevel(nLevel);
        if (nToCost > 0)
        {
            bNextLevelExist = true;
        }


        if ( nPoint < nToCost)
        {
            UIMsgBox.s_Instance.ShowMsg("技能点不够");
            return;
        }

        SetSkillPoint(eType, GetSkillPoint(eType) - nToCost); // 消耗天赋点

        m_CurSelectedLeaf.SetLevel(m_CurSelectedLeaf.GetLevel() + 1); // 本节点等级更新

        UpdateSubPanelInfo(m_CurSelectedLeaf);

        UIMsgBox.s_Instance.ShowMsg("升级成功");
        
        /*
        switch( m_CurSelectedLeaf.GetType())
        {
            case eBranchType.branch0:
                {
                    Main.s_Instance.UpdateRaise();

                    // (注：此处跟具体的逻辑玩法紧密相关，代码无法做到有通用性，只有见招拆招)
                    // 更新一下本节点的参数
                    sLeafConfig config = GetLeafConfig(eType, nLeafIndex);
                    int nNewLevel = m_CurSelectedLeaf.GetLevel();
                    float fCoinPromote = config.aryFloatParams[0];
                    int nPlanetId = config.aryIntParams[0];
                    int nTrackId = config.aryIntParams[1];
                    for (int i = 0; i < nNewLevel - 1; i++ )
                    {
                        fCoinPromote *= ( 1f + config.aryFloatParams[1]);
                    }

                    m_CurSelectedLeaf.SetSwitch(config.bSwitch);
                    m_CurSelectedLeaf.SetFloatParam(0, fCoinPromote);
                    m_CurSelectedLeaf.SetIntParam(0, nPlanetId);
                    m_CurSelectedLeaf.SetIntParam(1, nTrackId);

                    // 更新描述信息 
                    if (config.bSwitch)
                    {
                        _txtDesc.text = string.Format(config.szDesc, (fCoinPromote * 100).ToString("f2"));
                    }
                    else
                    {
                        _txtDesc.text = string.Format(config.szDesc, nPlanetId, nTrackId, (fCoinPromote * 100).ToString( "f2" ));

                    }
                }
                break;  
           

        } // end switch
        */

    }

    public int GetSkillPoint( eBranchType eType )
    {
        int nPoint = 0;

        if ( !m_dicSkillPoint.TryGetValue( eType, out nPoint ) )
        {
            return 0;
        }

        return nPoint;
    }

    public void SetSkillPoint(eBranchType eType, int nPoint)
    {
        m_dicSkillPoint[eType] = nPoint;

        switch( eType )
        {
            case eBranchType.branch0:
                {
                    _moneyBranch0.SetValue(nPoint);
                }
                break;
                
            case eBranchType.branch1:
                {
                    _moneyBranch1.SetValue(nPoint);
                }
                break;
            case eBranchType.branch2:
                {
                    _moneyBranch2.SetValue(nPoint);
                }
                break;
        } // end switch
    }


}// end class
