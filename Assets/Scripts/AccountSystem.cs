﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccountSystem : MonoBehaviour {

    public static AccountSystem s_Instance = null;

    public MoneyCounter[] m_aryGreenCash;

    double m_nGreenCash = 0; // 绿票。 绿票是绑定账号的，金币是绑定某个星球的

    Dictionary<int, int> m_dicTalentPointBuyTimes = new Dictionary<int, int>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        SetGreenCash( 50000000000d );  
        SetCoin(0, 50000000000d);
        SetCoin(1, 2000000000000d);
        SetCoin(2, 3000000000000d);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetGreenCash(double nValue)
    {
        m_nGreenCash = nValue;

        //Main.s_Instance._moneyGreenCash.SetValue ( nValue );
        Main.s_Instance.SetDiamond(nValue);

        for (int i = 0; i < m_aryGreenCash.Length; i++ )
        {
            MoneyCounter counter = m_aryGreenCash[i];
            if ( counter == null )
            {
                continue;
            }
            counter.SetValue( nValue );    
        }

        MapManager.s_Instance.SetAllDiamondValueText(nValue);

    }


    public double GetGreenCash()
    {
        return m_nGreenCash;
    }

    public double GetCoin( int nPlanetId  )
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        //  planet.SetCoin(nValue)
        return planet.GetCoin();
    }

    public void SetCoin(int nPlanetId, double nValue)
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        planet.SetCoin(nValue);

    }

    public double GetCoin()
    {
        return MapManager.s_Instance.GetCurPlanet().GetCoin();
    }

    public void SetCoin( double nValue )
    {
        MapManager.s_Instance.GetCurPlanet().SetCoin( nValue );

    }

    public void ChangeCoin( int nOp, double nValue )
    {
        double nCurCoin = GetCoin();
        if ( nOp > 0 )
        {
            nCurCoin += nValue;
        }
        else if (nOp < 0 )
        {
            nCurCoin -= nValue;
        }

        SetCoin(nCurCoin);

    }


    public void SetTalentPointBuyTime( int nType, int nBuyTimes  )
    {
        m_dicTalentPointBuyTimes[nType] = nBuyTimes;
    }

    public int GetTalentPointBuyTime(int nType)
    {
        int nBuyTimes = 0;
        if ( !m_dicTalentPointBuyTimes.TryGetValue( nType, out nBuyTimes) )
        {
            m_dicTalentPointBuyTimes[nType] = 0;
        }
        return nBuyTimes;
    }


} // end class
