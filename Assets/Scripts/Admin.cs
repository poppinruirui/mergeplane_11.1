﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Admin : MonoBehaviour {

    AdministratorManager.sAdminConfig m_Config ;

    int m_nColddownLeftTime = 0;
    int m_nDurationLeftTime = 0;

    bool m_bUsing = false;

    AdministratorManager.eUisngAdminStatus m_eStatus = AdministratorManager.eUisngAdminStatus.idle;

    System.DateTime m_dateSkillStartTime;
    float m_fColddownPercent = 0;

    UIAdministratorCounter m_BoundCounter = null;

    District m_BoundTrack = null;

    double m_nBuyPrice = 0; // 买入价 

    int[] m_aryIntParams = new int[8];


    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {

        SkillLoop();
      
	}

    public void Reset()
    {
        SetUsing( false );
        m_nColddownLeftTime = 0;
        m_nDurationLeftTime = 0;

        SetStatus(AdministratorManager.eUisngAdminStatus.idle);
    }

    public void SetBoundTrack( District track )
    {
        m_BoundTrack = track;
    }

    public void SetBoundCounter( UIAdministratorCounter counter )
    {
        m_BoundCounter = counter;
    }


    public UIAdministratorCounter GetBoundCounter()
    {
       return m_BoundCounter;
    }

    public void SetConfig( AdministratorManager.sAdminConfig config )
    {
        m_Config = config;
    }

    public void SetUsing( bool bUsing )
    {
        m_bUsing = bUsing;
    }

    public bool GetUsing()
    {
        return m_bUsing;
    }

    public AdministratorManager.sAdminConfig GetConfig()
    {
        return m_Config;
    }

    public void SetStatus( AdministratorManager.eUisngAdminStatus eStatus )
    {
        m_eStatus = eStatus;

    }

    public AdministratorManager.eUisngAdminStatus GetStatus()
    {
        return m_eStatus;
    }

    public void BeginCastSkill()
    {
        SetStatus(AdministratorManager.eUisngAdminStatus.casting);

        m_dateSkillStartTime = Main.GetSystemTime();
   
        m_nDurationLeftTime = m_Config.nDuration;

        switch ( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;
            case AdministratorManager.eAdminFuncType.automobile_accelerate:
                {
                    Main.s_Instance.AccelerateAll( 1f + m_Config.fValue );
                }
                break;
            case AdministratorManager.eAdminFuncType.gain_immediately:
                {
                    ReCalculateDPSForGain();
                    // “立即获取一段时间的收益”改成持续技能了
                    /*
                    int nDps = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
                    int nGain = (int)( nDps * m_Config.fValue );
                    int nPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
                    int nCurCoin = AccountSystem.s_Instance.GetCoin(nPlanetId);
                    AccountSystem.s_Instance.SetCoin(nPlanetId, nCurCoin + nGain);
                    UIMsgBox.s_Instance.ShowMsg( nDps + " DPS x " + m_Config.fValue + " 秒 = " + nGain);
                    EndCastSkill(); // 瞬时技能，一发出就结束
                    */
                }
                break;
        } // end switch
    }

    float m_fTimeElaspe = 0;
    void SkillLoop()
    {
        m_fTimeElaspe += Time.deltaTime;
        if (m_fTimeElaspe < 1f) // 每秒轮询一次 
        {
            return;
        }
        m_fTimeElaspe = 0f;

        Casting_SkillLoop();
        ColdDown_SkillLoop();

    }

    void Casting_SkillLoop()
    {
        if ( GetStatus() != AdministratorManager.eUisngAdminStatus.casting )
        {
            return;
        }

        // 时间跨度有两种算法，依据游戏逻辑选择合适的算法
        // 1、如果要求必须在线计时的，就用走帧的方法计时
        // 2、如果可以离线计时的，就用当前时间减去起始时间
        int nTimeSpan = (int)((Main.GetSystemTime() - m_dateSkillStartTime).TotalSeconds );

        switch( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.gain_immediately:
                {
                    SkillLoop_Gain();
                }
                break;
        } // end switch

        m_nDurationLeftTime = m_Config.nDuration - nTimeSpan;

        if (m_BoundTrack.IsCurTrack())
        {
            m_BoundCounter._progressbarColdDown.SetPercent(m_fColddownPercent);
            m_BoundCounter._progressbarColdDown.SetTextContent("施放中:" + m_nDurationLeftTime);
        }
        if (m_nDurationLeftTime <= 0)
        {
            EndCastSkill();
        }

    }

    void ReCalculateDPSForGain()
    {
        int nDps = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
        int nTotalGain = (int)(nDps * m_Config.fValue);
        int nGainPerRound = nTotalGain / m_Config.nDuration; // 在持续时间内，每秒获得多少
        SetIntParams(0, nGainPerRound);
    }

    void SkillLoop_Gain()
    {
        ReCalculateDPSForGain();

        int nGainOfThisRound = GetIntParams(0);
        AccountSystem.s_Instance.ChangeCoin(1, nGainOfThisRound);


        RichTiaoZi tiaoZi = ResourceManager.s_Instance.NewRichTiaoZi().GetComponent<RichTiaoZi>();
        tiaoZi.transform.SetParent(AdministratorManager.s_Instance._containerTiaoZi.transform);
        tiaoZi.SetPos(AdministratorManager.s_Instance.m_vecTiaoZiPos);
        tiaoZi.SetScale(1f);
        tiaoZi.SetScaleChangeParams( 1f, 1f );
        tiaoZi.Begin();
        tiaoZi.SetValue((int)nGainOfThisRound);

    }

    public int GetSkillCastingLeftTime()
    {
        return m_nDurationLeftTime;
    }

    public int GetSkillColdDownLeftTime()
    {
        return m_nColddownLeftTime;
    }

    void ColdDown_SkillLoop()
    {
        if (GetStatus() != AdministratorManager.eUisngAdminStatus.colddown)
        {
            return;
        }

        int nTimeSpan = (int)(Main.GetSystemTime() - m_dateSkillStartTime).TotalSeconds;
        m_nColddownLeftTime = m_Config.nColddown - nTimeSpan;
        int nColdDownLeftTime = m_Config.nColddown - nTimeSpan;
        m_fColddownPercent = (float)nColdDownLeftTime / (float)m_Config.nColddown;

        if (m_BoundTrack.IsCurTrack())
        {
            m_BoundCounter._progressbarColdDown.SetPercent(m_fColddownPercent);
            m_BoundCounter._progressbarColdDown.SetTextContent("冷却中:" + nColdDownLeftTime);
        }
        if (nTimeSpan >= m_Config.nColddown)
        {
            EndColdDown();
        }

    }

    void EndColdDown()
    {
        SetStatus(AdministratorManager.eUisngAdminStatus.idle);

        AdministratorManager.s_Instance.EndColdDown();
    }

    public float GetColdDownPercent()
    {
        return m_fColddownPercent; 
    }

    public void EndCastSkill()
    {
        if ( GetStatus() != AdministratorManager.eUisngAdminStatus.casting ) // 不要重复执行
        {
            return;
        }

        SetStatus(AdministratorManager.eUisngAdminStatus.colddown);
        m_dateSkillStartTime = Main.GetSystemTime();

        switch( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;
            case AdministratorManager.eAdminFuncType.automobile_accelerate:
                {
                    Main.s_Instance.StopAccelerateAll();
                }
                break;
        } // end switch


        AdministratorManager.s_Instance.EndCastSkill();
    }

    public void CancelCasting()
    {
        if (this.GetStatus() != AdministratorManager.eUisngAdminStatus.casting)
        {
            return;
        }

        this.EndCastSkill();
    }


    public void SetBuyPrice( double nValue )
    {
        m_nBuyPrice = nValue;
    }

    public double GetBuyPrice()
    {
        return m_nBuyPrice;
    }


    public void SetIntParams( int nIndex, int nValue )
    {
        m_aryIntParams[nIndex] = nValue;
    }

    public int GetIntParams(int nIndex )
    {
        return m_aryIntParams[nIndex];
    }

} // end class
