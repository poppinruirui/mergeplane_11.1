﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsManager : MonoBehaviour {

    public static AdsManager s_Instance = null;

    public float m_fTotalTime = 10000;
    float m_fCurTime = 0;

    public GameObject _panelAds;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _panelPreAds;
    public Text _txtAdsLeftTime;

    public Image _imgProgressBar;

    public float m_fBaseRaise = 2f;

    eAdsType m_eAdsType = eAdsType.common;

    int m_nCurProcessinPlanetId = 0;

    public enum eAdsType
    {
        common,
        bat_collect_offline,
        bat_ads_raise,
        collect_offline_profit_x2,
    };

    public void SetAdsType(eAdsType eType)
    {
        m_eAdsType = eType;
    }

    private void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        MainLoop();
	}

    public void OnClick_CloseAds()
    {
        _panelAds.SetActive(false);

        switch (m_eAdsType)
        {
            case eAdsType.common:
                {
                    //MapManager.s_Instance.GetCurDistrict().GainAdsRaise();
                    Bat_Ads_Raise();
                }
                break;

            case eAdsType.bat_collect_offline:
                {
                    CollectAllDistrictsOfflineGainOfThisPlanet();
                }
                break;
            case eAdsType.bat_ads_raise:
                {
                    Bat_Ads_Raise();
                }
                break;
            case eAdsType.collect_offline_profit_x2:
                {
                    District district = MapManager.s_Instance.GetCurDistrict();
                    float fOfflineProfit = Main.s_Instance.m_fOfflineProfit * 2;
                    Planet planet = MapManager.s_Instance.GetCurPlanet();
                    planet.SetCoin(planet.GetCoin() + (int)fOfflineProfit);
                    district.SetCurTotalOfflineGain(0);

                    Main.s_Instance.CollectOfflineProfitAnimation();
                }
                break;
          } // end switch
    }

   

    public void Bat_Ads_Raise()
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistricts = planet.GetDistrictsList();
        for (int i = 0; i < aryDistricts.Length; i++)
        {
            District district = aryDistricts[i];
            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                continue;
            }
            district.GainAdsRaise();
        }

        m_fCurTime += 60f;
        _imgProgressBar.fillAmount = m_fCurTime / m_fTotalTime;
    }

    // right here
    void CollectAllDistrictsOfflineGainOfThisPlanet()
    {
        string szDetails = "";
        float fTotal = 0;
        Planet planet = MapManager.s_Instance.GetPlanetById(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistricts = planet.GetDistrictsList();
        for (int i = 0; i < aryDistricts.Length; i++ )
        {
            District district = aryDistricts[i];
            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                szDetails += ("<Color=#C8C8C8>赛道未解锁</Color>\n");
                continue;
            }
            if (district == MapManager.s_Instance.GetCurDistrict())
            {
                szDetails += ("<Color=#C8C8C8>当前赛道</Color>\n");
                continue;
            }
            float fGainOfThisDistrict = district.CalculateOffLineProfit();
            fTotal += fGainOfThisDistrict;
            district.SetCurTotalOfflineGain(0);
            szDetails += ("<Color=#FEE834>" +  fGainOfThisDistrict.ToString("f0") + "</Color>" + "\n" );
        } // end for 

 
        float fDouble = fTotal * 2;
        MapManager.s_Instance._txtBatOfflineGain.text = fTotal.ToString( "f0" ) + " X 2 = " + fDouble.ToString( "f0" );
        MapManager.s_Instance._txtBatOfflineDetail.text = szDetails;

        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(true );

        planet.SetCoin( planet.GetCoin() + (int)fTotal);
    }

    public void OnClick_ClosebatCollectionPanel()
    {
        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(false);

    }

    public float GetAdsRaise()   
    {
        return m_fBaseRaise * ( 1f + ScienceTree.s_Instance.GetAdsCoinRaise() );
    }

    public void OnClick_OpenAds()
    {
        _panelPreAds.SetActive( true );
    }

    public void PlayAds()
    {
        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
    }

    public void OnClick_WatchAds()
    {
        _panelAds.SetActive(true);
        SetAdsType(eAdsType.common);
        // _panelPreAds.SetActive(false);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();
    }

    public void OnClick_ClosePreAds()
    {
        _panelPreAds.SetActive(false);
    }

    public void OnClick_OpenAds_CollectAllDistrcitOfflineGainsOfThisPlanet()
    {
        _panelAds.SetActive(true);
        SetAdsType(eAdsType.bat_collect_offline);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();

    }

    public void OnClick_OpenAds_BatAdsRaise()
    {
        _panelAds.SetActive(true);
        SetAdsType(eAdsType.bat_ads_raise);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();
    }

    float m_fMainLoopTimeElapse = 0f;
    public void MainLoop()
    {
        m_fMainLoopTimeElapse += Time.deltaTime;
        if (m_fMainLoopTimeElapse < 1f)
        {
            return;
        }
        m_fMainLoopTimeElapse = 0;

        Planet[] aryPlanets = MapManager.s_Instance.GetPlanetList();
        if (aryPlanets == null)
        {
            return;
        }
        for (int i = 0; i < aryPlanets.Length; i++ )
        {
            Planet planet = aryPlanets[i];
            District[] aryDistricts = planet.GetDistrictsList();
            for (int j = 0; j < aryDistricts.Length; j++ )
            {
                District district = aryDistricts[j];
                district.AdsRaiseTimeLoop();
                if (district == MapManager.s_Instance.GetCurDistrict())
                {
                    ShowLeftTime(district.GetAdsLeftTime());
                }
            } // end for j
        } // end for i

    }

    public void ShowLeftTime( float fLeftTime )
    {
        if (fLeftTime <= 0)
        {
            _txtAdsLeftTime.text = "";
        }
        else
        {
            _txtAdsLeftTime.text = "剩余时间：" + fLeftTime.ToString();
        }
    }


} // end class
